import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
  { path: 'menu-page/:id', loadChildren: './pages/menu-page/menu-page.module#MenuPagePageModule' },
  { path: 'cart', loadChildren: './pages/cart/cart.module#CartPageModule' },
  { path: 'addresslist', loadChildren: './pages/addresslist/addresslist.module#AddresslistPageModule' },
  { path: 'add-address', loadChildren: './pages/add-address/add-address.module#AddAddressPageModule' },
  { path: 'about-us', loadChildren: './pages/about-us/about-us.module#AboutUsPageModule' },
  { path: 'faq', loadChildren: './pages/faq/faq.module#FaqPageModule' },
  { path: 'my-orders', loadChildren: './pages/my-orders/my-orders.module#MyOrdersPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'food-menu', loadChildren: './pages/food-menu/food-menu.module#FoodMenuPageModule' },
  { path: 'my-profile', loadChildren: './pages/my-profile/my-profile.module#MyProfilePageModule' },
  { path: 'header', loadChildren: './header/header.module#HeaderPageModule' },
  { path: 'check-out', loadChildren: './check-out/check-out.module#CheckOutPageModule' }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
