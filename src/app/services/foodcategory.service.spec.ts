import { TestBed } from '@angular/core/testing';

import { FoodcategoryService } from './foodcategory.service';

describe('FoodcategoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FoodcategoryService = TestBed.get(FoodcategoryService);
    expect(service).toBeTruthy();
  });
});
