import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FoodcategoryService {

  url = 'https://www.jackies.in/test/index.php/v1/Api/getcategorylist';
  constructor(private http: HttpClient) { }
 results: any ;
  searchData(){
  	  this.http.post(`${this.url}`, this.results).subscribe((res)=>{
            //console.log(res);
            this.results = res;
        });
    //this.results =  this.http.post(`${this.url}`).
    //pipe(
    //  results =>results.category
    //);
    console.log(this.results)
    return this.results
  }
}

