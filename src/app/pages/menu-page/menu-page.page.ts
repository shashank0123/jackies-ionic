import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-menu-page',
  templateUrl: './menu-page.page.html',
  styleUrls: ['./menu-page.page.scss'],
})
export class MenuPagePage implements OnInit {

  qty: number = 0;
	groups: any;
	id
	url = 'https://admin.jackies.in/index.php/v1/Api/getSubCategoryList';
	results: any ;
	category_name: any ;
	subcategories: [] ;
	constructor(private route: ActivatedRoute,private http: HttpClient) {
	    
	}
  ngOnInit(): void {
  	this.id = this.route.snapshot.paramMap.get("id")
  	let poster = new FormData();
  	// console.log(this.id);
  	poster.append('category_id', this.id);
  	

  	this.http.post(`${this.url}`,poster, this.results).subscribe((res)=>{
			console.log(res['category'][0]['category_name']);
			this.category_name = res['category'][0].category_name;
			this.subcategories = res['category'][0]['items'];

      this.setDefaultQty();
		});

  }


  addToCart(menu_id, type){
  	let formdata1 = new FormData();
  	formdata1.append('menu_id', menu_id);
  	formdata1.append('type', type);
  	

  	this.http.post(`${this.url}`,formdata1, this.results).subscribe((res)=>{
			console.log(res['category'][0]['category_name']);
			this.category_name = res['category'][0].category_name;
			this.subcategories = res['category'][0]['items'];
		});
  }

// for  add product in cart

   increamentQty (index:number, quantity:number) {

     // if(this.subcategories[index]['qty'] && this.subcategories[index]['qty'] <= 0    ) {
     //   this.subcategories[index]['qty'] = 0;

     // }
     this.subcategories[index]['qty']++;


   }
    decreamentQty (index:number, quantity:number) {

     if(this.subcategories[index]['qty'] && this.subcategories[index]['qty'] >= 0    ) {
      this.subcategories[index]['qty']--;

     }
    


   }

   // set qty 0 for all category
   setDefaultQty() {
     this.subcategories.forEach( (item, index) => {
       Object.assign(item, {qty: 0});
     })
   }


}

