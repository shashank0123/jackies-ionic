import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
qty: number = 0;
  constructor() { }

  ngOnInit() {
  }


 // for  add product in cart

   increamentQty ( ) {
        this.qty++;
  }

   decreamentQty( ) {
   	// var qt = this.qty;
     if(this.qty >= 1 ){
      this.qty--;
  }

  }



}
