 import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import  { Router  } from '@angular/router';

import { HttpClient } from '@angular/common/http';

@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
	styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

	constructor(private http: HttpClient, private router:Router ,) {
	}


	cartDetails( ){
this.router.navigate(['/cart']);

  }

	url = 'https://admin.jackies.in/index.php/v1/Api/getcategorylist';
	results: any ;

	ngOnInit() { 
		this.http.post(`${this.url}`, this.results).subscribe((res)=>{
			this.results = res['category'];
		});
	}
}
