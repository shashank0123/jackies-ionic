import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Menu',
      url: '/home',
      img: 'assets/icon/menu.png'
    },
    {
      title: 'About Us',
      url: '/about-us',
      img: 'assets/icon/about.png'
    },
    {
      title: 'Cart',
      url: '/cart',
      img: 'assets/icon/cart.png'
    },
    {
      title: 'My-Orders',
      url: '/my-orders',
      img: 'assets/icon/orders.png'
    },
    {
      title: 'FAQ',
      url: '/faq',
      img: 'assets/icon/faq.png'
    },
     {
      title: 'My-Address',
      url: '/addresslist',
      img: 'assets/icon/address.png'
    },
    {
      title: 'My-Profile',
      url: '/my-profile',
      img: 'assets/icon/profile.png'
    },
    {
      title: 'Logout',
      url: '/login',
      img: 'assets/icon/logout.png'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
